# SWANS (Spoken Word Analyser SWANS)
<img align="right" src="docs/logo.png" width="25%">

SWANS is an easy to set up and use web-application using the framework Django and therefore being (manly) written in Python! It is designed to be used in speech training to give trainees a better understanding of their voice and expression through visual feedback using color gradients based on traffic light colors. It closes the gap between understanding speech and scientific results. The student can easily determine how he*she did. The base for all the calculations made is [PRAAT](https://www.fon.hum.uva.nl/praat/) — an established tool for voice analysis. Recordings of a short text (e.g., "The North Wind and the Sun") have to be done in as quiet a room as possible and with a fixed mouth-microphone distance of approx. 30 cm (12 inches). Any software that fit's your training work flow can be used to do the recording – use lossless formats for best results.

### Background

This free and open source software is the result of a bachelor thesis focusing on visual feedback in speech and voice training. It has been written for and at the faculty of media informatics at the University of Regensburg. Its main goal was to present a new and intuitive way of understanding and teaching speech. Media informatics is an interdisciplinary field, therefore it was possible to create SWANS together with experts from the faculty of Oral Communication and Speech Training of the Center of Language and Communication at the University of Regensburg. The resulting product offered for download and usage here can be used in any kind of speech training – and since it's based on the well known software [PRAAT](https://www.fon.hum.uva.nl/praat/), the measurements are reliable and reproducible.

## Features (v1.0)
<img align="right" src="docs/swans-results.png" width="45%">

* Mode pitch and intensity analysis based on algorithms of [PRAAT](https://www.fon.hum.uva.nl/praat/)
* Instant and understandable feedback for your clients
* Default ranges for pitch and intensity based on the latest research
* Manually configurable target pitch range
* Full result breakdown for the coach. Hidden in the default view to not confuse your client, but only the click of button away
* Export of generated high-res graphics and full result breakdown
* Runs on any UNIX-based computer/server

## Download
You can find the [latest stable release here!](https://gitlab.com/PilleA/swans/-/releases)

## Installation
For the installation of all dependencies you can in the base directory just use:

```bash
pipenv install
```
That should go ahead and download all libraries used in this project. If you'd prefer not to work with pipenv, you can also use:

```bash
pip install -r requirements.txt
```

For flawless creation of the gradient labels, the two fonts saved at the font directory need to be installed on your server. You can also grab them from their origins:

* [OpenSans](https://fonts.google.com/specimen/Open+Sans?sidebar.open=true&selection.family=Open+Sans)
* [FreeFont](https://ftp.gnu.org/gnu/freefont/)

Just unzip them and move the `*.ttf` files to `~/.local/share/fonts`. Then just empty the font cache with

```bash
fc-cache -f -v
```

## Usage
To test the application on your server, you can start it with

```bash
pipenv run swans/manage.py runserver
```
That will start a development server so that the application is available (at least per default) at `http://localhost:8000`

For actual deployment on a production server, please refer to the [Django Docs](https://docs.djangoproject.com/en/3.1/howto/deployment/)

## License
This software is proudly [licensed](LICENSE) under GNU GPLv3 – meaning free as in freedom. Even though selected with great care, its dependencies might be released under a different license or change their licensing in the future.
