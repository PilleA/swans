#!/usr/bin/env python3
import glob
import json
import os
import shutil
from pathlib import Path
import time
from zipfile import ZipFile
from django.conf import settings

from . import convert
from .blackswans import blackswans
from .whiteswans import whiteswans

# Base directory for all following file operations
media_folder = settings.MEDIA_ROOT
static_folder = settings.STATICFILES_DIRS


def get_directory(user_id):
    # Returns user directory based on given user_id
    directory = media_folder + "/" + str(user_id)
    return directory


def check_validity(file):
    # ffmpeg pretty much takes any audio format out there. This function makes sure it really only is an audio file
    allowed_formats = ['.flac', '.mp3', '.wav', '.ogg', '.opus', '.aac', '.m4a', '.alac', '.vorbis', '.wma', '.aiff',
                       '.ac3', '.m4r']
    file_ending = os.path.splitext(str(file))[1]
    print(file_ending, allowed_formats)
    if file_ending.lower() in allowed_formats:
        return True
    else:
        return False


def create(user_id: int, file):
    # Generates case folder for new user and saves uploaded file in it
    directory = get_directory(user_id)
    os.mkdir(directory)
    os.chdir(directory)
    file_ending = os.path.splitext(str(file))[1]
    with open("orig-audio" + file_ending, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)

    # Adding time stamp file for folder deletion after session ended
    with open("creation.txt", mode='a') as file:
        file.write(str(time.time()))


def start_conversions(user_id):
    os.chdir(get_directory(user_id))
    try:
        # Converts user's audio file to ogg vorbis for later playback in browser.
        # Called first, because it is a relatively fast process
        convert.vorbis((get_file(user_id, 'orig_audio')), 'audio')
        # Converting to flac enables blackswans to read nearly any audio format since ffmpeg has a
        # almost endless range of supported filetypes. After it's flac, blackswans can analyze it.
        convert.flac((get_file(user_id, 'orig_audio')), 'audio')
        # Removes original audio because it's no longer needed
        os.remove(get_file(user_id, 'orig_audio'))
    except:
        # In case ffmpeg fails
        'Not an audio file'


def purge(user_id):
    # Deletes user directory. Call with caution.
    shutil.rmtree(get_directory(user_id))


def delete_visual(user_id):
    # Deletes generated visualisation. Can be called e.g. user goes back into configuration after already having results
    os.chdir(get_directory(user_id))
    os.remove(get_file(user_id, 'intensity_png'))
    os.remove(get_file(user_id, 'pitch_png'))
    os.remove(get_file(user_id, 'config_json'))
    if Path(get_file(user_id, 'export_zip')).is_file():
        os.remove(get_file(user_id, 'export_zip'))


def start_analysis(user_id):
    # Starts analysis. Needs to have a FLAC file in folder.
    os.chdir(get_directory(user_id))  # changes into folder to write results to the correct location
    blackswans.analyze((glob.glob('*.flac')[0]))


def start_visualisation(user_id):
    # Starts generating visual results. Needs config, result and templates to execute functions
    os.chdir(get_directory(user_id))  # changes into folder to later write pngs to the correct location
    results = get_file(user_id, 'result_json')
    config = get_file(user_id, 'config_json')
    template = get_file(user_id, 'template_svg')
    template_table = get_file(user_id, 'template_table_svg')
    whiteswans.generate(config, results, template, template_table)


def save_visual_config(user_id, config_json):
    # Writes out given config into user folder
    os.chdir(get_directory(user_id))
    data = config_json
    with open('config.json', 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)


def has_been_analyzed(user_id):
    # Status check on already being analyzed
    if Path(get_file(user_id, 'result_json')).is_file():
        return True
    else:
        return False


def has_been_visualized(user_id):
    # Status check on already having generated visualisation
    if Path(get_file(user_id, 'intensity_png')).is_file():
        return True
    else:
        return False


def get_file(user_id, filetype: str):
    # Function returns desired file from corresponding user folder. It can either give back a relative link
    # for client usage (marked with _link) or the global file for server-side processing (marked with _filetype)
    directory = get_directory(user_id)
    if filetype == 'result_json':
        return directory + "/result.json"
    elif filetype == 'result_csv':
        return directory + "/result.csv"
    elif filetype == 'ogg_link':
        return str(settings.MEDIA_URL) + str(user_id) + "/" + glob.glob('*.ogg', recursive=directory)[0]
    elif filetype == 'config_json':
        return directory + "/config.json"
    elif filetype == 'pitch_link':
        return str(settings.MEDIA_URL) + str(user_id) + "/" + "pitch.png"
    elif filetype == 'pitch_png':
        return directory + "/pitch.png"
    elif filetype == 'intensity_link':
        return str(settings.MEDIA_URL) + str(user_id) + "/" + "intensity.png"
    elif filetype == 'intensity_png':
        return directory + "/intensity.png"
    elif filetype == 'table_link':
        return str(settings.MEDIA_URL) + str(user_id) + "/" + "table.png"
    elif filetype == 'template_svg':
        return static_folder[0] + '/upload/template.svg'
    elif filetype == 'template_table_svg':
        return static_folder[0] + '/upload/template_table.svg'
    elif filetype == 'flac':
        return directory + "/" + glob.glob('*.flac', recursive=directory)[0]
    elif filetype == 'orig_audio':
        return directory + "/" + glob.glob('orig-audio.*', recursive=directory)[0]
    elif filetype == 'export_zip':
        return directory + "/export.zip"
    elif filetype == 'export_link':
        return str(settings.MEDIA_URL) + str(user_id) + "/" + "export.zip"
    else:
        return 'File not found'


def export(user_id):
    # Creates export.zip file. ZIP needs some resources even though the files
    # should be relatively small. Call only on user's wish.

    os.chdir(get_directory(user_id))

    def get_all_file_paths():
        # initializing empty file paths list
        file_path_array = []
        # Getting all files
        flac = glob.glob('*.flac')[0]
        result = 'result.csv'
        pitch = 'pitch.png'
        intensity = 'intensity.png'

        # Adding to the array
        file_path_array.append(flac)
        file_path_array.append(result)
        file_path_array.append(pitch)
        file_path_array.append(intensity)
        # returning all file paths
        return file_path_array

    # calling function to get all file paths in the directory
    file_paths = get_all_file_paths()

    # writing files to a zipfile
    with ZipFile('export.zip', 'w') as zip:
        for file in file_paths:
            zip.write(file)


def delete_old_cases():
    # "Ättestupa"-cleanup function reading creation time of case and deciding if it is
    # old and not needed anymore and therefore can be deleted.
    # The process doesn't take long because it's only dealing with text files and creation dates. Can be called anytime.
    current_time = time.time()
    cases_to_delete = []
    for root, dirs, files in os.walk(media_folder):
        for file in files:
            if file.endswith(".txt"):
                creation_time = os.path.getctime(
                    file)  # TODO: Version 1.1 should read date from file not the the os creation of it.
                # If the case is older than one day, it will be deleted.
                # Could be set shorter in case of running into issues
                if (current_time - creation_time) // (24 * 3600) >= 1:
                    cases_to_delete.append(root)
    for i in cases_to_delete:
        # Executes the deletion
        shutil.rmtree(i)
