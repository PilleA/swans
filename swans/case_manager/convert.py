import ffmpeg


# Converter script for audio files into different formats. Can easily be extended to convert to other
# formats using according ffmpeg tags in new functions. Since ffmpeg is very flexible it can deal
# with nearly any input audio file.

def output(input_file, output_file: str, codec: str, bitrate: str, sample_rate: int):
    # Wrapper function that starts conversion after being set up in defining functions underneath
    output_file = ffmpeg.output(input_file, output_file, **{'c:a': codec, 'b:a': bitrate, 'ar': sample_rate})
    ffmpeg.run(output_file)


def vorbis(file, name: str):
    input_file = ffmpeg.input(file)
    codec = 'libvorbis'
    bitrate = '128k'
    sample_rate = 44100
    new_name = name + '.ogg'
    output(input_file, new_name, codec, bitrate, sample_rate)


def flac(file, name: str):
    input_file = ffmpeg.input(file)
    codec = 'flac'
    bitrate = '320k'  # Redundant for FLAC but needed for output function
    sample_rate = 44100
    new_name = name + '.flac'
    output(input_file, new_name, codec, bitrate, sample_rate)


def mp3(file, name: str):
    input_file = ffmpeg.input(file)
    codec = 'libmp3lame'
    bitrate = '160k'
    sample_rate = 44100
    new_name = name + '.mp3'
    output(input_file, new_name, codec, bitrate, sample_rate)
