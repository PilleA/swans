#!/usr/bin/env python3

# Takes input values and writes them to the desired format
# TODO: Let them take arrays and not a full list of arguments
def csv(floor_fq, ceiling_fq, pitch_mean, pitch_strdv_f0, pitch_median, pitch_mode, intensity_mean, intensity_max,
        intensity_min):
    import pandas as pd
    values = {
        'Measurement': ['Used Floor FQ (Hz)', 'Used Ceiling FQ (Hz)', 'Median Pitch (Hz)', 'Mean Pitch (Hz)',
                        'Standard Deviation F0', 'Mode Pitch (Hz)', 'Mean Intensity (dB)', 'Max. Intensity (dB)',
                        'Min. Intensity (dB)'],
        'Result': [floor_fq, ceiling_fq, pitch_median, pitch_mean, pitch_strdv_f0, pitch_mode, intensity_mean,
                   intensity_max, intensity_min]
    }

    df = pd.DataFrame(values, columns=['Measurement', 'Result'])
    df.to_csv('result.csv')


def json(floor_fq, ceiling_fq, pitch_mean, pitch_strdv_f0, pitch_median, pitch_mode, intensity_mean, intensity_max,
         intensity_min):
    import json
    data = {'pitch': [], 'intensity': []}
    data['pitch'].append({
        'floor_fq': floor_fq,
        'ceiling_fq': ceiling_fq,
        'pitch_mean': pitch_mean,
        'pitch_strdv_f0': pitch_strdv_f0,
        'pitch_median': pitch_median,
        'pitch_mode': pitch_mode
    })
    data['intensity'].append({
        'intensity_mean': intensity_mean,
        'intensity_max': intensity_max,
        'intensity_min': intensity_min
    })

    with open('result.json', 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)
