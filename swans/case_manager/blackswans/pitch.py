import parselmouth
from parselmouth.praat import call
import numpy as np
import statistics


def generate(sound, f0min, f0max):
    # Creating a praat pitch object using cross correlation which is better suited for voice research than
    # autocorrelation (Mayer, J. (2017). Phonetische Analysen mit Praat. Ein Handbuch für Ein- und Umsteiger
    # (2017/09 Aufl.). http://praatpfanne.lingphon.net/downloads/praat_manual.pdf)
    pitch_object = call(sound, "To Pitch (cc)", 0, f0min, 15,
                        'yes', 0.03, 0.45, 0.01, 0.35, 0.14, f0max)
    return pitch_object


def median(pitch_object, unit):
    # Getting the median pitch
    median_pitch = call(pitch_object, "Get quantile", 0, 0, 0.5, unit)
    return median_pitch


def mean_f0(pitch_object, unit):
    # Getting the mean pitch
    mean_pitch = call(pitch_object, "Get mean", 0, 0, unit)
    return mean_pitch


def stdv_f0(pitch_object, unit):
    # Getting standard f0 deviation
    stdvf0 = call(pitch_object, "Get standard deviation", 0, 0, unit)
    return stdvf0


def mode(pitch_object):
    # Getting mode pitch
    pitch_array_raw = pitch_object.selected_array['frequency']
    pitch_array_cleaned = np.round(pitch_array_raw[pitch_array_raw != 0.0], 0)
    mode_pitch = statistics.mode(list(pitch_array_cleaned))
    return mode_pitch
