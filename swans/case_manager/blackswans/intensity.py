from parselmouth.praat import call


def generate(sound, min_pitch):
    # producing praat intensity object
    intensity_object = call(sound, "To Intensity", min_pitch, 0.0, 'yes')
    return intensity_object


def mean(intensity_object):
    # getting mean intensity in dB
    mean_intensity = call(intensity_object, "Get mean", 0.0, 0.0, 'energy')
    return mean_intensity


def max_intensity(intensity_object):
    # getting maximum intensity in dB
    maximum = call(intensity_object, "Get maximum", 0.0, 0.0, 'Parabolic')
    return maximum


def min_intensity(intensity_object):
    # getting minimum intensity in dB
    minimum = call(intensity_object, "Get minimum", 0.0, 0.0, 'Parabolic')
    return minimum
