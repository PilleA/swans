#!/usr/bin/env python3
import parselmouth
from . import get_settings
from . import pitch
from . import intensity
from . import expert_results
from . import export_data as export

unit = "Hertz"


def analyze(file):
    # Generates a praat sound file that can be used by any part of the analyzing
    sound = parselmouth.Sound(file)

    # Calculates  floor and ceiling parameters for analysis with algorithm by Hirst (2011)
    min_pitch_fq, max_pitch_fq = get_settings.pitch(sound)

    def pitch_analysis():
        pitch_object = pitch.generate(sound, min_pitch_fq, max_pitch_fq)
        pitch_mean = pitch.mean_f0(pitch_object, unit)
        pitch_strdv_f0 = pitch.stdv_f0(pitch_object, unit)
        pitch_median = pitch.median(pitch_object, unit)
        pitch_mode = pitch.mode(pitch_object)
        return pitch_mean, pitch_strdv_f0, pitch_median, pitch_mode

    def intensity_analyses():
        intensity_object = intensity.generate(sound, min_pitch_fq)
        intensity_mean = intensity.mean(intensity_object)
        intensity_max = intensity.max_intensity(intensity_object)
        intensity_min = intensity.min_intensity(intensity_object)
        return intensity_mean, intensity_max, intensity_min

    pitch_mean, pitch_strdv_f0, pitch_median, pitch_mode = pitch_analysis()
    intensity_mean, intensity_max, intensity_min = intensity_analyses()
    # expert_results.create(sound)  # TODO: Fully implement feature for Version 1.1
    export.csv(min_pitch_fq, max_pitch_fq, pitch_mean, pitch_strdv_f0, pitch_median, pitch_mode, intensity_mean,
               intensity_max, intensity_min)
    export.json(min_pitch_fq, max_pitch_fq, pitch_mean, pitch_strdv_f0, pitch_median, pitch_mode, intensity_mean,
                intensity_max, intensity_min)
