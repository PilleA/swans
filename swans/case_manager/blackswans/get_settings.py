import parselmouth
from parselmouth.praat import call

# Instead of guesstimating vocal range, Hirst (2011) has come up with a method of calculating it (Hirst, D. (2011).
# The analysis by synthesis of speech melody: from data to models. Journal of Speech Sciences, 1(1), 55–83. The
# method is also described in Mayer, J. (2017). Phonetische Analysen mit Praat. Ein Handbuch für Ein- und Umsteiger (
# 2017/09 Aufl.). http://praatpfanne.lingphon.net/downloads/praat_manual.pdf)


starting_floor = 50
starting_ceiling = 700


def pitch(sound):
    # Generating pitch object using the normal method, not cc or ac. This is faster and more efficient and "enough"
    # for this analysis
    pitch_object = call(sound, "To Pitch", 0.01, starting_floor, starting_ceiling)

    # Getting Q1
    q1 = call(pitch_object, "Get quantile", 0, 0, 0.25, "Hertz")

    # Getting Q3
    q3 = call(pitch_object, "Get quantile", 0, 0, 0.75, "Hertz")

    # Calculating floor value
    floor = round((q1 * 0.75), 0)

    # Calculating ceiling value
    ceiling = round((q3 * 2.5), 0)
    return floor, ceiling
