import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import threading

# FEATURE FOR VERSION 1.1

# Creating some graphics for experts using guide
# https://parselmouth.readthedocs.io/en/stable/examples/plotting.html

def create(sound_file):
    sns.set()  # Use seaborn's default style to make attractive graphs
    plt.rcParams['figure.dpi'] = 150

    def draw_waveform():
        plt.figure()
        plt.plot(sound_file.xs(), sound_file.values.T)
        plt.xlim([sound_file.xmin, sound_file.xmax])
        plt.xlabel("time [s]")
        plt.ylabel("amplitude")
        plt.savefig("waveform.png")

    def draw_spectro_intensity():
        def draw_spectrogram(spectro, dynamic_range=70):
            X, Y = spectro.x_grid(), spectro.y_grid()
            sg_db = 10 * np.log10(spectro.values)
            plt.pcolormesh(X, Y, sg_db, vmin=sg_db.max() - dynamic_range, cmap='afmhot')
            plt.ylim([spectro.ymin, spectro.ymax])
            plt.xlabel("time [s]")
            plt.ylabel("frequency [Hz]")

        def draw_intensity(intens):
            plt.plot(intens.xs(), intens.values.T, linewidth=3, color='w')
            plt.plot(intens.xs(), intens.values.T, linewidth=1)
            plt.grid(False)
            plt.ylim(0)
            plt.ylabel("intensity [dB]")

        intensity = sound_file.to_intensity()
        spectrogram = sound_file.to_spectrogram()
        plt.figure()
        draw_spectrogram(spectrogram)
        plt.twinx()
        draw_intensity(intensity)
        plt.xlim([sound_file.xmin, sound_file.xmax])
        plt.savefig("spectrogram.png")

    process_one = threading.Thread(target=draw_waveform())
    process_one.start()
    process_two = threading.Thread(target=draw_spectro_intensity())
    process_two.start()
