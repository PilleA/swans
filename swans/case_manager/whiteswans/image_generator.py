import numpy as np
from . import color_map


def svg(target_low, target_high, result_value, template_file):
    template_svg = open(template_file, 'rt').read()

    def replace_value(file, old_value, new_value):
        # Goes through given text file (svg) and replaces some value with another value
        new_file = file.replace(str(old_value), str(new_value))
        return new_file

    def calculate_min_max():
        # Calculates value left and right the gradient based on user input
        midpoint = np.round(((target_low + target_high) / 2), 0)
        green_sector_div = midpoint - target_low
        minimum = target_low - green_sector_div
        maximum = target_high + green_sector_div
        return minimum, maximum

    def correct_position_two_digits(arrow_position):
        # Aligns the values in the middle of the arrow
        if len(str(int(result_value))) <= 2:
            new_position = arrow_position + 80
            return new_position
        elif len(str(int(result_value))) > 2:
            new_position = arrow_position + 40
            return new_position
        else:
            return arrow_position

    def generate_svg():
        minimum, maximum = calculate_min_max()
        if result_value < minimum:
            # Catches values lower than on scale possible
            arrow_position_factor = 0.0
        else:
            # Calculates factor so that result is represented on gradient scale
            arrow_position_factor = (result_value - minimum) / (maximum - minimum)
        arrow_position = arrow_position_factor * 846  # Gradient width in pixel
        arrow_color = color_map.rgb(arrow_position_factor)

        # Set min gradient value
        left_value_svg = replace_value(template_svg, 'MIN', int(minimum))
        # Set left gradient value
        right_value_svg = replace_value(left_value_svg, 'MAX', int(maximum))
        # Set bottom value
        bottom_value_svg = replace_value(right_value_svg, 'BOTTOM', int(result_value))
        # Set bottom position
        bottom_position_svg = replace_value(bottom_value_svg, 'BOT_TEXT_POS',
                                            correct_position_two_digits(arrow_position))
        # Set arrow color
        arrow_color_svg = replace_value(bottom_position_svg, 'ARROW_COLOR', arrow_color)
        # Set arrow position
        final_svg = replace_value(arrow_color_svg, 'POSITION', arrow_position)
        return final_svg

    return generate_svg()
