#!/usr/bin/env python3
import json
import numpy as np
from cairosvg import svg2png
from . import image_generator
from . import table_generator


def generate(config_json, result_json, template_file, template_table):
    # Saving generated SVG to PNG using utility svg2png
    def export_to_png(svg_file, resolution: int, name: str):
        output_name = name + ".png"
        svg2png(bytestring=bytes(svg_file, 'UTF-8'), dpi=resolution, write_to=output_name)

    # Reading in given json file
    with open(result_json) as result_file:
        result = json.load(result_file)
    with open(config_json) as config_file:
        config = json.load(config_file)

    # Generating svg files
    pitch_svg = image_generator.svg(config['pitch'][0]['pitch_target_low'], config['pitch'][0]['pitch_target_high'],
                                    result['pitch'][0]['pitch_mode'], template_file)
    intensity_svg = image_generator.svg(config['intensity'][0]['intensity_target_low'],
                                        config['intensity'][0]['intensity_target_high'],
                                        np.round(result['intensity'][0]['intensity_mean'], 0), template_file)
    table_svg = table_generator.svg(template_table, result)

    # Exporting corresponding png files with 300 dpi
    export_to_png(pitch_svg, 300, 'pitch')
    export_to_png(intensity_svg, 300, 'intensity')
    export_to_png(table_svg, 300, 'table')
