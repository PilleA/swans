def svg(template_file, result):
    template_svg = open(template_file, 'rt').read()

    def replace_value(file, old_value, new_value):
        new_file = file.replace(str(old_value), str(new_value))
        return new_file

    def generate_svg():
        # Generates table svg based on the template file. It's using image_generator's way of
        # replacing values in svg files
        floor_svg = replace_value(template_svg, 'used_floor', result['pitch'][0]['floor_fq'])
        ceiling_svg = replace_value(floor_svg, 'used_ceiling', result['pitch'][0]['ceiling_fq'])
        median_pitch_svg = replace_value(ceiling_svg, 'median_pitch', result['pitch'][0]['pitch_median'])
        mean_pitch_svg = replace_value(median_pitch_svg, 'mean_pitch', result['pitch'][0]['pitch_mean'])
        std_dv_svg = replace_value(mean_pitch_svg, 'std_dv', result['pitch'][0]['pitch_strdv_f0'])
        mode_pitch_svg = replace_value(std_dv_svg, 'mode_pitch', result['pitch'][0]['pitch_mode'])
        mean_intensity_svg = replace_value(mode_pitch_svg, 'mean_intensity', result['intensity'][0]['intensity_mean'])
        max_intensity_svg = replace_value(mean_intensity_svg, 'max_intensity', result['intensity'][0]['intensity_max'])
        final_svg = replace_value(max_intensity_svg, 'min_intensity', result['intensity'][0]['intensity_min'])
        return final_svg

    return generate_svg()
