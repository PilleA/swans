import numpy as np


def rgb(position):
    # Matches RGB color value to given position factor in percent (<= 1)

    def hex_to_rgb(hex_value: str):
        # Returns RGB value corresponding to given HEX
        rgb_value = tuple(int(hex_value[i:i + 2], 16) for i in (0, 2, 4))
        return rgb_value

    def get_hex(position_value):
        # Steps calculated based on the amount of colors available.
        # Could be thought of as the "resolution" of the gradient
        step = 0.02702702703
        total_steps = 1 / step
        # Values hardcoded because they don't change. http://www.perbang.dk/rgbgradient/
        color_array = ['DC3545', 'DF443E', 'E35337', 'E76331', 'EB722A', 'EF8224', 'F3911D', 'F7A117', 'FBB010',
                       'FFC00A', 'E7BD10', 'CFBA17', 'B7B81D', '9FB524', '87B22A', '6FB031', '57AD37', '3FAA3E',
                       '27A845', '3FAA3E', '57AD37', '6FB031', '87B22A', '9FB524', 'B7B81D', 'CFBA17', 'E7BD10',
                       'FFC00A', 'FBB010', 'F7A117', 'F3911D', 'EF8224', 'EB722A', 'E76331', 'E35337', 'DF443E',
                       'DC3545']

        # Creates array with all the steps possible based on step size defined above
        step_array = []
        for i in np.arange(0, (total_steps + step), 1):
            step_array.append(step * i)

        # Algorithm that matches the given position value to the corresponding color value
        i = 0
        while i <= len(step_array):
            if position_value < step_array[1]:
                # Min value returns extreme (red)
                return color_array[0]
            elif position_value >= 1:
                # Max value (due to rounding) returns extreme (red)
                return color_array[len(color_array) - 1]
            elif step_array[i] < position_value < step_array[i + 1]:
                # Default case that matches position to value of the color array
                return color_array[i]
            else:
                i += 1

    return hex_to_rgb(get_hex(position))
