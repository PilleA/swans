from django.http import HttpResponse
from django.shortcuts import render, redirect
import sys
from wsgiref.util import FileWrapper
import threading

# Enabling relative import from parent directory
sys.path.append(sys.path[0] + "/..")
from case_manager import case_manager
import uuid
from django.db import models


class UUIDModel(models.Model):
    # Creates UUID for referencing the case
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)


def upload(request):
    # Generation of session key used for referencing to user and saving it to Django's session cookie
    request.session['user_id'] = str(UUIDModel().id)

    # Saves uploaded file and creates case after form is filled
    if request.method == 'POST' and 'submit':
        # Checking if file was selected before clicking upload button
        try:
            uploaded_file = request.FILES['document']
        except:
            return redirect('/swans')
        if case_manager.check_validity(uploaded_file):
            case_manager.create(request.session['user_id'], uploaded_file)
            request.session['status'] = 'upload'  # Updating cookie with the application's status
            return redirect('/swans/waiting')

    return render(request, 'upload.html')


def configuration(request):
    # Link file for playback in browser
    file_link = case_manager.get_file(request.session['user_id'], 'ogg_link')

    # Only gets called after form is filled. If not it's just returning the view.
    if request.method == 'POST':

        # Reacting to button press 'back'
        if 'back' in request.POST:
            case_manager.purge(request.session['user_id'])
            return redirect('/swans')

        checked_box = request.POST.getlist('targetRange')

        def set_targets():
            # Reading form's values and returning corresponding target values
            checked_box_string = str(checked_box)[2:-2]
            if checked_box_string == 'option1':
                # Male speaker
                target_low = 89
                target_high = 175
                return target_low, target_high
            elif checked_box_string == 'option2':
                # Natural voice
                target_low = 145
                target_high = 175
                return target_low, target_high
            elif checked_box_string == 'option3':
                # Female voice
                target_low = 164
                target_high = 260
                return target_low, target_high
            elif checked_box_string == 'option4':
                targets = [request.POST['manual_low'], request.POST['manual_high']]
                if int(targets[0]) == '' or int(targets[1]) == '':
                    # User input was empty. Abort value reading.
                    return 'False input'

                target_int_array = [int(numeric_string) for numeric_string in targets]
                if min(target_int_array) <= 30 or max(target_int_array) >= 1200:
                    # Checking against non-plausible inputs
                    return 'False input'
                else:
                    # If values are valid, then save and return values
                    # Checks for min and max in case user switched fields around
                    target_low = min(target_int_array)
                    target_high = max(target_int_array)
                    return target_low, target_high
            else:
                # Catch all false user inputs.
                return 'False input'

        def save_config(pitch_low, pitch_high, intensity_low, intensity_high):
            # Takes all given values and hands them over to the case manager for saving them into the user's directory
            config = {'pitch': [], 'intensity': []}
            config['pitch'].append({
                'pitch_target_low': pitch_low,
                'pitch_target_high': pitch_high,
            })
            config['intensity'].append({
                'intensity_target_low': intensity_low,
                'intensity_target_high': intensity_high,
            })
            case_manager.save_visual_config(request.session['user_id'], config)

        if set_targets() == 'False input':
            # Something went wrong, directs user back to config
            return redirect('/swans/configuration')
        else:
            # Function call to rad all values and saving them
            pitch_target_low, pitch_target_high = set_targets()
            intensity_target_low, intensity_target_high = 55, 69  # TODO: Feature manual user intensity input
            save_config(pitch_target_low, pitch_target_high, intensity_target_low, intensity_target_high)
            request.session['status'] = 'config'
            return redirect('/swans/waiting')
    else:
        # Default respond to view call
        return render(request, 'configuration.html', {'linkOgg': file_link})


def results(request):
    # Setting up view by loading all elements. Default returning page, executes functions on button clicks
    file_link = case_manager.get_file(request.session['user_id'], 'ogg_link')
    pitch_link = case_manager.get_file(request.session['user_id'], 'pitch_link')
    intensity_link = case_manager.get_file(request.session['user_id'], 'intensity_link')
    table_link = case_manager.get_file(request.session['user_id'], 'table_link')

    if request.method == 'GET':
        if 'back' in request.GET:
            # Back button clicked
            case_manager.delete_visual(request.session['user_id'])
            return redirect('/swans/configuration')
        elif 'export' in request.GET:
            # Export button clicked
            case_manager.export(request.session['user_id'])
            file_path = case_manager.get_file(request.session['user_id'], 'export_zip')
            wrapper = FileWrapper(open(file_path, 'rb'))
            response = HttpResponse(wrapper, content_type='application/force-download')
            response['Content-Disposition'] = 'inline; filename=' + 'export.zip'
            return response
    return render(request, 'results.html',
                  {'pitch': pitch_link, 'intensity': intensity_link, 'table': table_link, 'linkOgg': file_link})


def waiting(request):
    # Checks status of application periodically through JS in the HTML file.
    if request.session['status'] == 'upload':
        # Coming from upload page, starting conversion
        conversion_thread = threading.Thread(target=case_manager.start_conversions(request.session['user_id']))
        conversion_thread.start()
        request.session['status'] = 'conversion'
        reason = 'Upload is in progress...'
        return render(request, 'waiting.html', {'reason': reason})
    elif request.session['status'] == 'conversion':
        # Conversion successful, starting analysis
        analysis_thread = threading.Thread(target=case_manager.start_analysis(request.session['user_id']))
        analysis_thread.start()
        request.session['status'] = 'analysis'
        reason = 'Analysis is in progress...'
        return render(request, 'waiting.html', {'reason': reason})
    elif case_manager.has_been_analyzed(request.session['user_id']) and request.session['status'] == 'analysis':
        # Analysis successful, directing to configuration page
        return redirect('/swans/configuration')
    elif case_manager.has_been_analyzed(request.session['user_id']) and request.session['status'] == 'config':
        # Coming from configuration page, starting analysis
        visualisation_thread = threading.Thread(target=case_manager.start_visualisation(request.session['user_id']))
        visualisation_thread.start()
        request.session['status'] = 'visualize'
        reason = 'Visualization is in progress...'
        return render(request, 'waiting.html', {'reason': reason})
    elif case_manager.has_been_visualized(request.session['user_id']):
        # Visualisation successful, directing to results page.

        # Takes a chance and cleans up media directory by removing old cases. Function could be called at any time and
        # might be separated feature called periodically in versions 1.2 and above
        case_manager.delete_old_cases()
        request.session['status'] = 'result'
        return redirect('/swans/results')
    else:
        # Catchall clause in case the condition checks all fail.
        # Page will reload in a couple seconds with hopefully a successful if-clause-check.
        return render(request, 'waiting.html', {'reason': 'Thank you for your patience!'})
