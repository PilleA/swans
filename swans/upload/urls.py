from django.urls import path

from . import views

urlpatterns = [
    path('', views.upload, name='swans'),
    path('configuration', views.configuration, name='configuration'),
    path('results', views.results, name='results'),
    path('waiting', views.waiting, name='waiting'),
]
