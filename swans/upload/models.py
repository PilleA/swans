import os
import uuid
from django.conf import settings
from django.db import models


def voice_path(_, file_name):
    return os.path.join(settings.MEDIA_ROOT, str(uuid.uuid4()), file_name)


class AudioFile(models.Model):
    audio_file = models.FileField()


class Voice(models.Model):
    voice = models.FileField(upload_to=voice_path, default='none')
