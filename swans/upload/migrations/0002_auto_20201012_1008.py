# Generated by Django 3.1.1 on 2020-10-12 10:08

from django.db import migrations, models
import upload.models


class Migration(migrations.Migration):

    dependencies = [
        ('upload', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='voice',
            name='date',
        ),
        migrations.RemoveField(
            model_name='voice',
            name='filename',
        ),
        migrations.RemoveField(
            model_name='voice',
            name='filepath',
        ),
        migrations.RemoveField(
            model_name='voice',
            name='uid',
        ),
        migrations.AddField(
            model_name='voice',
            name='voice',
            field=models.FileField(default='none', upload_to=upload.models.voice_path),
        ),
    ]
