from django import forms


class ConfigForm(forms.Form):
    pitch_target_low = forms.CheckboxInput()
    pitch_target_manual_low = forms.CharField(max_length=1)
    pitch_target_manual_high = forms.CharField(max_length=1)
