from django.shortcuts import render, redirect


def home(request):
    return render(request, 'home.html')


def legal(request):
    return render(request, 'legal.html')
